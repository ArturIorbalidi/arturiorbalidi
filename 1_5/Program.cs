﻿using System;

namespace _1_5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите год своего рождения");
            string xstr = Console.ReadLine();
            if (int.TryParse(xstr, out int x))
            {
                x = x % 12;
                xstr = x.ToString();
                switch (xstr)
                {
                    case "4": Console.WriteLine("Год крысы"); break;
                    case "5": Console.WriteLine("Год быка"); break;
                    case "6": Console.WriteLine("Год тигра"); break;
                    case "7": Console.WriteLine("Год кролика"); break;
                    case "8": Console.WriteLine("Год дракона"); break;
                    case "9": Console.WriteLine("Год змеи"); break;
                    case "10": Console.WriteLine("Год лошади"); break;
                    case "11": Console.WriteLine("Год козы"); break;
                    case "0": Console.WriteLine("Год обезьяны"); break;
                    case "1": Console.WriteLine("Год петуха"); break;
                    case "2": Console.WriteLine("Год собаки"); break;
                    case "3": Console.WriteLine("Год свиньи"); break;
                }
            }
            else
            {
                Console.WriteLine("Введено не корректное значение");
            }
        }
    }
}
